<?php

require_once ('Database.php');

class Crud extends Database {

    public function get(): array
    {
        $query = "SELECT * FROM tasks";
        return $this -> connection -> query($query) -> fetch_all(MYSQLI_ASSOC);
    }

    public function insert($task): bool
    {
        $query = "INSERT INTO tasks (`task`) VALUES ('$task')";
        return $this -> connection -> query($query);
    }

    public function delete($id): bool
    {
        $query = "DELETE FROM tasks WHERE id = $id";
        return $this -> connection -> query($query);
    }

    public function check($id): bool
    {
        $query = "UPDATE tasks SET `checked` = NOT `checked` WHERE id = $id";
        return $this -> connection -> query($query);
    }

}
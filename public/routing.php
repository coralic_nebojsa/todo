<?php
/*
 * Simple router for routing requests
 */
require_once(dirname(__DIR__) . "/classes/Tasks.php");
$tasks = new Tasks();

$method = $_SERVER['REQUEST_METHOD'];
$route = $_SERVER['REQUEST_URI'];

if($method != "POST" && in_array($route, ['/todo/addTask', '/todo/deleteTask', '/todo/checkTask'])) {

    header('Location: /todo');
}

switch ($route) {

    case '/todo/addTask':

        $tasks -> insertTask($_POST['task']);
        header('Location: /todo');
        break;

    case '/todo/deleteTask':

        echo $tasks -> deleteTask($_POST['task_id']);
        break;

    case '/todo/checkTask':

        return $tasks -> checkTask($_POST['task_id']);

    default:
        header('Location: /todo');
}


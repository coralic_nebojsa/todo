<?php

class Database {

    public $connection;
    private $host = 'localhost';
    private $username = 'root';
    private $password = '';
    private $database = 'todo';

    public function __construct()
    {
        $this -> connection = $this -> connect();
    }

    private function connect() {

        $connection = mysqli_connect($this -> host, $this -> username, $this -> password, $this -> database)
            or die("Couldn't connect");

        return $connection;
    }
}
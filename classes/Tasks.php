<?php

require_once ('Crud.php');

class Tasks extends Crud {

    public function listTasks(): array
    {
        return $this -> get();
    }

    public function insertTask($task): bool
    {
        return $this -> insert($task);
    }

    public function deleteTask($task): bool
    {
        $this -> delete($task);
        return '123';
    }

    public function checkTask($task): bool
    {
        return  $this -> check($task);
    }
}


<?php

require_once(dirname(__DIR__) . "/classes/Tasks.php");

$tasks = new Tasks();
$listTasks = $tasks -> listTasks();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php') ?>
</head>
<body style='overflow-x: hidden!important; max-width: 100%;'>

<p class="display-4 d-flex justify-content-center"><strong>TO-DO</strong></p>

    <div class="row p-5">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Task</h3>
                    </div>
                    <form action="addTask" method="POST">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="task">Task</label>
                                        <input type="text" id="task" name="task" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-submit"><i class="fas fa-plus"></i> Add Task</button>
                        </div>
                    </form>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Tasks</h3>
                    </div>
                    <div class="card-body">
                        <table id="tasks_table" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Task</th>
                                <th>Created At</th>
                                <th>Check</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                                foreach ($listTasks as $task)
                                {
                                    $checked = $task['checked'] ? 'checked' : '';

                                    echo '<tr>
                                            <td>'. $task['task'] .'</td>
                                            <td>'. $task['created_at'] .'</td>
                                            <td><input data-id="'. $task['id'] .'" type="checkbox" class="form-check-input" '. $checked .'></td>
                                            <td><button id="'. $task['id'] .'" type="submit" class="btn btn-danger btn-submit" onclick="deleteTask(this)">Delete</button></td>
                                          </tr>';
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</body>

<script>

    function deleteTask(button)
    {
        let task_id = $(button).attr('id');

        $.post( "deleteTask", {task_id: task_id}, function( data ) {

            if(data) {
                alert('Successfully deleted.');
                location.reload();
            }
            else {
                alert('Something went wrong!');
            }
        });
    }

    $(".form-check-input").change( function(){

        let task_id = $(this).data('id');

        $.post( "checkTask", {task_id: task_id}, function( data ) {});
    });

</script>